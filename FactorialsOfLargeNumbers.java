//{ Driver Code Starts
//Initial Template for Java

import java.io.*;
import java.util.*;

class GfG
{
    public static void main(String args[])
    {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while(t-->0)
        {
            int N = sc.nextInt();
            Solution ob = new Solution();
            ArrayList<Integer> ans = ob.factorial(N);
            for (Integer val: ans) 
                System.out.print(val); 
            System.out.println();
        }   
    }
}
// } Driver Code Ends


class Solution {
    static ArrayList<Integer> factorial(int N){
        ArrayList<Integer> result = new ArrayList<>();
        result.add(1); // Initialize with 1 (factorial of 0)

        // Multiply the factorial value with numbers from 2 to N
        for (int i = 2; i <= N; i++) {
            multiply(result, i);
        }

        // Reverse the list to get digits in the correct order
        Collections.reverse(result);
        return result;
    }

    static void multiply(ArrayList<Integer> result, int x) {
        int carry = 0;

        for (int i = 0; i < result.size(); i++) {
            int prod = result.get(i) * x + carry;
            result.set(i, prod % 10); // Store the last digit
            carry = prod / 10; // Update carry for next multiplication
        }

        // Store remaining carry if any
        while (carry != 0) {
            result.add(carry % 10);
            carry /= 10;
        }
    }
}
